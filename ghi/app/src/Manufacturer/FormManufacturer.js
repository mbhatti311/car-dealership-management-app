import React, {useState, useEffect } from 'react';
import ListManufacturer from './ListManufacturer'

function FormManufacturer() {
    const [manufacturers, setManufacturers] = useState([])

    const [name, setName] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
      fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
          const newManufacturer = await response.json();

          setName('');
        }
      }


    return (
        <div className="row" id="background">
          <div className="offset-3 col-6">
            <div className="pt-5">
              <h1>Create a Manufacturer</h1>
              <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
              <div className="pt-5">
              <ListManufacturer></ListManufacturer>
              </div>
            </div>
          </div>
        </div>
      );

}

export default FormManufacturer
