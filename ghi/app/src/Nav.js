import { NavLink } from 'react-router-dom';
import React from 'react';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Mercedes Benz of Houston</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
             {/* <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/models">Models</NavLink>
            </li> */}
            <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers/new">Add a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/inventory">Vehicle Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/models/new">Create a model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="services/new">Create Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="services/technicians/new">Create Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="services/history">Appointment History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/salesperson">Add a salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/customer">Add a customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/new">Record a new sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/history">Sales person history</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    // <nav id="navbar" className="navbar">
    // <ul>
    //   <li><a className="nav-link scrollto active" href="#hero">Home</a></li>
    //   <li><a className="nav-link scrollto" href="#about">About</a></li>
    //   <li><a className="nav-link scrollto" href="#services">Services</a></li>
    //   <li><a className="nav-link scrollto " href="#portfolio">Portfolio</a></li>
    //   <li><a className="nav-link scrollto" href="#team">Team</a></li>
    //   <li className="dropdown"><a href="#"><span>Drop Down</span> <i className="bi bi-chevron-down" /></a>
    //     <ul>
    //       <li><a href="#">Drop Down 1</a></li>
    //       <li className="dropdown"><a href="#"><span>Deep Drop Down</span> <i className="bi bi-chevron-right" /></a>
    //         <ul>
    //           <li><a href="#">Deep Drop Down 1</a></li>
    //           <li><a href="#">Deep Drop Down 2</a></li>
    //           <li><a href="#">Deep Drop Down 3</a></li>
    //           <li><a href="#">Deep Drop Down 4</a></li>
    //           <li><a href="#">Deep Drop Down 5</a></li>
    //         </ul>
    //       </li>
    //       <li><a href="#">Drop Down 2</a></li>
    //       <li><a href="#">Drop Down 3</a></li>
    //       <li><a href="#">Drop Down 4</a></li>
    //     </ul>
    //   </li>
    //   <li><a className="nav-link scrollto" href="#contact">Contact</a></li>
    // </ul>
    // <i className="bi bi-list mobile-nav-toggle" />
    // </nav>
  )
}

export default Nav;
