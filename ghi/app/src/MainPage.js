import React from 'react';

function MainPage() {
  return (
    <div>
    {/* ======= Hero Section ======= */}
    <section id="hero">
      <div className="hero-container" data-aos="zoom-in" data-aos-delay={100}>
        <h1>Vehicle Management Portal</h1>
        <h2>Manage vehicle inventory, sales, and maintenance appointments all in one place!</h2>
        <a href="/inventory" className="btn-get-started">Manage Inventory</a>
        <a href="/sales/new" className="btn-get-started">Manage Sales</a>
        <a href="/services/new" className="btn-get-started">Manage Service Appointments</a>
      </div>
    </section>{/* End Hero Section */}
  </div>
  );
}

export default MainPage;
